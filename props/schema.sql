/*******************************************************************************
 *  Copyright 2013 Jason Sipula, others                                        *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

DROP DATABASE IF EXISTS compmarkdb;

CREATE DATABASE compmarkdb;

USE compmarkdb;

REVOKE ALL PRIVILEGES, GRANT OPTION
FROM 'compmarkuser'@'%';

DROP USER 'compmarkuser'@'%';

GRANT ALTER, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, INSERT, LOCK TABLES, SELECT, UPDATE
ON compmarkdb.*
TO 'compmarkuser'@'%'
IDENTIFIED BY 'Cr@zy11!!';

FLUSH PRIVILEGES;

DROP TABLE IF EXISTS comp;
DROP TABLE IF EXISTS base;

CREATE TABLE base
(
   id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   sku VARCHAR(28) UNIQUE NOT NULL,
   url VARCHAR(2000) NOT NULL,
   price DECIMAL(6,2) NOT NULL,
   sales_unit INT UNSIGNED NOT NULL
) ENGINE=InnoDB ;

CREATE TABLE comp
(
   id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   sku VARCHAR(28) UNIQUE NOT NULL,
   url VARCHAR(2000) NOT NULL,
   price DECIMAL(6,2) NOT NULL,
   price2 DECIMAL(6,2),
   price3 DECIMAL(6,2),
   price4 DECIMAL(6,2),
   sales_unit INT UNSIGNED NOT NULL
) ENGINE=InnoDB ;

CREATE TABLE xref
(
   id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   base_ref INT UNSIGNED NOT NULL REFERENCES base,
   comp_ref INT UNSIGNED NOT NULL REFERENCES comp
) ENGINE=InnoDB ;
