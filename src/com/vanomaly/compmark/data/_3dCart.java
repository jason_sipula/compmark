/*******************************************************************************
 *  Copyright 2013 Jason Sipula, others                                        *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package com.vanomaly.compmark.data;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import net.snakedoc.jutils.Config;
import net.snakedoc.jutils.ConfigException;
import net.snakedoc.jutils.database.MSSQL;

import org.apache.log4j.Logger;

public class _3dCart extends Database {
    
    /** Setup working objects */
    MSSQL mssql = null;
    
    private final String sqlSelect = "SELECT products.price FROM products WHERE id = ?";
    private PreparedStatement psSelect;
    
    /**
     * Constructor assemble DB objects and fetch properties
     */
    public _3dCart() {
        this.logger = Logger.getLogger(_3dCart.class);
        this.cfg = new Config("props/conf.properties");
        try {
            this.mssql = new MSSQL(cfg.getConfig("BASE_DB_HOST") + ":" + cfg.getConfig("BASE_DB_PORT") 
                                    + ";databaseName=" + cfg.getConfig("BASE_DB_NAME"), 
                                    cfg.getConfig("BASE_DB_USER"), cfg.getConfig("BASE_DB_PASS"));
        } catch (ConfigException e) {
            logger.fatal("Failed to read config!", e);
        }
        cfg.loadConfig("props/log4j.properties");
    }

    /**
     * Connect to 3dCart (MSSQL DB)
     */
    /* (non-Javadoc)
     * @see com.vanomaly.compmark.data.Database#openConnection()
     */
    public boolean openConnection() {
        try {
            mssql.openConnection();
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            logger.fatal("Failed to open connection to 3dCart!", e);
            return false;
        }
    }
    
    /**
     * Disconnect session from 3dCart (MSSQL DB)
     */
    /* (non-Javadoc)
     * @see com.vanomaly.compmark.data.Database#closeConnection()
     */
    public boolean closeConnection() {
        try {
            mssql.closeConnection();
            return true;
        } catch (SQLException e) {
            logger.warn("Failed to close connection to 3dCart!", e);
            return false;
        }
    }
    
    /**
     * Initializes Prepared Statements
     */
    /* (non-Javadoc)
     * @see com.vanomaly.compmark.data.Database#prepareStatements()
     */
    public boolean prepareStatements() {
        
        if (this.isStatementSet()) {
            
            return true; // success
            
        } else {
            
            try {
                this.psSelect = mssql.getConnection().prepareStatement(this.sqlSelect);
            } catch (SQLException e) {
                this.logger.error("Failed to set statements!", e);
                return false; // failure
            }
            
            return true; // success
            
        }
    }
    
    /**
     * Checks if prepared statements are set or not (!= null)
     * 
     * @return True is set (!= null)
     */
    public boolean isStatementSet() {
        
        return (this.psSelect != null) ? true : false;
        
    }
}
