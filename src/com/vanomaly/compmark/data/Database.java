/*******************************************************************************
 *  Copyright 2013 Jason Sipula, others                                        *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package com.vanomaly.compmark.data;

import net.snakedoc.jutils.Config;

import org.apache.log4j.Logger;

/**
 * Class used to communicate with the underlying database application.
 *
 */
public abstract class Database {
	
	/** Setup working objects */
	protected Logger logger = null;
	protected Config cfg = null;
	
	/**
	 * Opens Database Connection.
	 * 
	 * @return True if success.
	 */
	public abstract boolean openConnection();
	
	/**
	 * Closes Database Connection.
	 * 
	 * @return True if success.
	 */
	public abstract boolean closeConnection();
	
	/**
	 * Initialized Prepared Statements.
	 * 
	 * @return True if success.
	 */
	public abstract boolean prepareStatements();
	
	/**
	 * Checks if instance logger is set.
	 * 
	 * @return True if success. (!= null)
	 */
	public boolean isLoggerSet() {
	    return (this.logger != null) ? true : false;
	}
	
	/**
	 * Checks if instance Config is set.
	 * 
	 * @return True if success. (!= null)
	 */
	public boolean isConfigSet() {
	    return (this.cfg != null) ? true : false;
	}

}
