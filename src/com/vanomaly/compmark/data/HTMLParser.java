/*******************************************************************************
 *  Copyright 2013 Jason Sipula, others                                        *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package com.vanomaly.compmark.data;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class HTMLParser {
	
	private final String[] elementStr;
	private Document doc;
	private Elements[] elements;

	/**
	 * Constructor
	 * 
	 * @param elements String Array of target elements to search page for
	 * @throws IOException If failed to connect to specified URL.
	 */
	public HTMLParser (String[] elementStr, String url) throws IOException {
		this.elementStr = elementStr;
		try {
			this.doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			throw new IOException ("Failed to connect to " + url, e);
		}
	}
	
	/**
	 * Block default constructor from use.
	 * 
	 * @throws Throwable Blocks this default constructor from use.
	 */
	private HTMLParser() throws Throwable {
		throw new Throwable("Don't use this constructor!");
	}
	
	public void parsePage() throws Exception {
		
		if ( ! isElementStrSet() ) {
			
			throw new Exception ("Parser exception! Elements are not set!");
			
		}
		
		else if ( ! isElementsSet() ) {
			
			this.setElements();
			
		}
		
		int q = 0;
		for (Element elm : this.elements[q]) {
			if ( ! (elm.text().equalsIgnoreCase("")) ) {
				System.out.println(elm.text()); // remove this
				// process logic and store to db here
			}
			q++;
		}
		
	}
	
	/**
	 * Sets Elements in array from Element String array.
	 */
	private void setElements() {
		this.elements = new Elements[this.elementStr.length];
		for (int i = 0; i < this.elementStr.length; i++) {
			this.elements[i] = this.doc.select(elementStr[i]);
		}
	}
	
	/**
	 * Sets instance Document
	 * 
	 * @param doc Document to set instance Document to.
	 */
	public void setDoc(Document doc) {
		this.doc = doc;
	}
	
	/**
	 * Returns instance Document.
	 * 
	 * @return Instance Document.
	 */
	public Document getDoc() {
		return this.doc;
	}
	
	/**
	 * Checks if instance Document is set.
	 * 
	 * @return True if instance Document is instantiated.
	 */
	public boolean isDocSet() {
		return (this.doc != null) ? true : false;
	}
	
	/**
	 * Checks if the Element String array is set.
	 * 
	 * @return True if instance of Element String array is set.
	 */
	public boolean isElementStrSet() {
		return (this.elementStr != null) ? true : false;
	}
	
	/**
	 * Checks if the Elements Array is set.
	 * 
	 * @return True if instance of Element Array is set.
	 */
	public boolean isElementsSet() {
		return (this.elements != null) ? true : false;
	}
	
}
