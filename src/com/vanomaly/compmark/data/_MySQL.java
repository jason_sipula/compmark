/*******************************************************************************
 *  Copyright 2013 Jason Sipula, others                                        *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package com.vanomaly.compmark.data;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import net.snakedoc.jutils.Config;
import net.snakedoc.jutils.ConfigException;
import net.snakedoc.jutils.database.MySQL;

import org.apache.log4j.Logger;

public class _MySQL extends Database {
    /** Setup working objects */
    MySQL mysql = null;
    
    // set SQL strings
    private final String sqlInsertBase = "INSERT INTO base (sku, url, price, sales_unit) VALUES (? , ? , ? , ?)";
    private final String sqlInsertComp = "INSERT INTO comp (sku, url, price, price2, price3, price4, sales_unit) VALUES (? , ? , ? , ? , ? , ? , ?)";
    private final String sqlInsertXref = "INSERT INTO xref (base_ref, comp_ref) VALUES (? , ?)";
    private final String sqlSelectBaseId = "SELECT base.id FROM base WHERE sku = ?";
    private final String sqlSelectCompId = "SELECT comp.id FROM base WHERE sku = ?";
    
    // create null'ed Prepared Statements for later use.
    private PreparedStatement psInsertBase;
    private PreparedStatement psInsertComp;
    private PreparedStatement psInsertXref;
    private PreparedStatement psSelectBaseId;
    private PreparedStatement psSelectCompId;
    
    /**
     * Constructor assemble DB objects and fetch properties
     */
    public _MySQL() {
        this.logger = Logger.getLogger(MySQL.class);
        this.cfg = new Config("props/conf.properties");
        try {
            this.mysql = new MySQL(cfg.getConfig("DB_HOST") + ":" + cfg.getConfig("DB_PORT") 
                                    + ";databaseName=" + cfg.getConfig("DB_NAME"), 
                                    cfg.getConfig("DB_USER"), cfg.getConfig("DB_PASS"));
        } catch (ConfigException e) {
            logger.fatal("Failed to read config!", e);
        }
        cfg.loadConfig("props/log4j.properties");
    }

    /**
     * Connect to Database Storage (MySQL DB)
     */
    /* (non-Javadoc)
     * @see com.vanomaly.compmark.data.Database#openConnection()
     */
    public boolean openConnection() {
        try {
            mysql.openConnection();
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            logger.fatal("Failed to open connection to 3dCart!", e);
            return false;
        }
    }
    
    /**
     * Disconnect session from Database (MySQL DB)
     */
    /* (non-Javadoc)
     * @see com.vanomaly.compmark.data.Database#closeConnection()
     */
    public boolean closeConnection() {
        try {
            mysql.closeConnection();
            return true;
        } catch (SQLException e) {
            logger.warn("Failed to close connection to 3dCart!", e);
            return false;
        }
    }
    
    /**
     * Initializes Prepared Statements
     */
    /* (non-Javadoc)
     * @see com.vanomaly.compmark.data.Database#prepareStatements()
     */
    public boolean prepareStatements() {
        
        if (this.isStatementsSet()) {
            
            return true; // success
            
        } else {
            
            try { // set prepared statements
                this.psInsertBase = mysql.getConnection().prepareStatement(this.sqlInsertBase);
                this.psInsertComp = mysql.getConnection().prepareStatement(this.sqlInsertComp);
                this.psInsertXref = mysql.getConnection().prepareStatement(this.sqlInsertXref);
                this.psSelectBaseId = mysql.getConnection().prepareStatement(this.sqlSelectBaseId);
                this.psSelectCompId = mysql.getConnection().prepareStatement(this.sqlSelectCompId);
            } catch (SQLException e) {
                this.logger.error("Failed to set statements!", e);
                return false; // failure
            }
            
            return true; // success
            
        }
    }
    
    /**
     * Checks if prepared statements are set or not (!= null)
     * 
     * @return True is set (!= null)
     */
    public boolean isStatementsSet() {
        
        return ( (this.psInsertBase != null) && (this.psInsertComp != null) 
                && (this.psInsertXref != null) && (this.psSelectBaseId != null) 
                    && (this.psSelectCompId != null) ) ? true : false;
    }
}
