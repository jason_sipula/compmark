/*******************************************************************************
 *  Copyright 2013 Jason Sipula, others                                        *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package com.vanomaly.compmark.javafx.view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import net.snakedoc.jutils.Config;
import net.snakedoc.jutils.ConfigException;

/**
 * View used in the GUI.
 *
 */
public class View extends Application {

	private Config cfg = new Config("props/conf.properties");
	//private String BACKGROUND_COLOR = cfg.getConfig("BACKGROUND_COLOR");
	
	public static void main(String[] args) {
		launch(View.class, args);
	}
	
	@Override
	public void start(Stage primaryStage) {
		
		Scene primaryScene = new Scene(this.getMainBorderPane());
		primaryStage.setScene(primaryScene);
		try {
			primaryStage.setTitle(cfg.getConfig("APPLICATION_TITLE"));
		} catch (ConfigException e) {
			e.printStackTrace();
		}
		
//		primaryStage.getIcons().add(getIcon());
		
		primaryStage.show();
		
	}
	
	private BorderPane getMainBorderPane() {
		
		BorderPane mainBorderPane = new BorderPane();
		mainBorderPane.setMinSize(1000, 600);
		
		return mainBorderPane;
		
	}
	
//	private Image getIcon() {
//		Image ico = null;
//		
//		try {
//			
//			ico = new Image(new FileInputStream("resources/logo.png"));
//			
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//		
//		return ico;
//	}
	
	private String getBackgroundColor() throws ConfigException {
		
		return cfg.getConfig("BACKGROUND_COLOR");
		
	}
	
}
