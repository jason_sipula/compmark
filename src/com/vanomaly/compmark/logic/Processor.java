/*******************************************************************************
 *  Copyright 2013 Jason Sipula, others                                        *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package com.vanomaly.compmark.logic;

public class Processor {

    
    
    /**
     * Strip String of specified character(s). Optional to
     * trim all leading and trailing whitespace characters.
     * 
     * @param input Input original String.
     * @param strip Character(s) to strip from the original String.
     * @param replace Character(s) to replace the striped character(s) with.
     * @param trim True if leading and trailing whitespace should be stripped as well.
     * @return Stripped/Cleaned String.
     */
    private String stripString(String input, String strip, String replace, boolean trim) {
        
        if (trim) {
            return input.replace(strip, replace).trim();
        } else {
            return input.replace(strip, replace);
        }
        
    }
    
}
